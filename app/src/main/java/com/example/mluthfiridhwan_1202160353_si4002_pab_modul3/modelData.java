package com.example.mluthfiridhwan_1202160353_si4002_pab_modul3;

public class modelData {
    private String nama, pekerjaan, jenisKelamin;

    public modelData(String nama, String pekerjaan, String jenisKelamin) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.jenisKelamin = jenisKelamin;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
}
